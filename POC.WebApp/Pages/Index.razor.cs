﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace POC.WebApp.Pages
{
    public partial class Index
    {
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private string? errorMessage;
        private Models.Person person = new Models.Person();
        private int personId;
        private readonly HashSet<int> personIds = new HashSet<int>();
        private string ApiVersion
        {
            get => this.Config.ApiBaseUrl;
            set => this.Config.ApiBaseUrl = value;
        }

        [Inject] private Services.ApiService Api { get; init; } = default!;
        [Inject] private Services.ConfigService Config { get; init; } = default!;
        [Inject] private ISnackbar Snackbar { get; init; } = default!;

        public void Dispose()
        {
            this.tokenSource.Cancel();
            this.tokenSource.Dispose();
        }

        protected override async Task OnInitializedAsync()
        {
            Models.Person[] people = await this.Api
                .GetPeople(this.tokenSource.Token)
                .ConfigureAwait(false);

            foreach (Models.Person person in people)
            {
                this.personIds.Add(person.Id);
            }
        }

        private void ClearPerson()
        {
            this.person.Gender = default;
            this.person.Id = default;
            this.person.IsAlive = default;
            this.person.Name = default;
        }

        private async Task OnClearClicked()
        {
            this.ClearPerson();
            await Task.CompletedTask;
        }

        private async Task OnSaveClicked()
        {
            try
            {
                this.errorMessage = null;

                await this.Api
                    .CreatePerson(this.person, this.tokenSource.Token)
                    .ConfigureAwait(false);

                this.personIds.Add(this.person.Id);
                this.ClearPerson();
                this.Snackbar.Add("Success!", Severity.Success);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                this.errorMessage = ex.Message;
            }
        }

        private async Task OnGetClicked()
        {
            try
            {
                this.errorMessage = null;

                this.person = await this.Api
                    .GetPerson(this.personId, this.tokenSource.Token)
                    .ConfigureAwait(false);

                this.personId = default;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                this.errorMessage = ex.Message;
            }
        }
    }
}
