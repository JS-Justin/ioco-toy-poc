﻿namespace POC.WebApp.Rpc
{
    public abstract class Request
    {
        public Guid TraceId { get; set; } = Guid.NewGuid();
    }

    public abstract class Request<TModel> : Request
        where TModel : new()
    {
        public TModel Payload { get; set; } = default!;
    }
}
