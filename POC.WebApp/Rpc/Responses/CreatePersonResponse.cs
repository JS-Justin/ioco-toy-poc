﻿namespace POC.WebApp.Rpc.Responses
{
    public class CreatePersonResponse : Response
    {
        public int PersonId { get; set; }
    }
}
