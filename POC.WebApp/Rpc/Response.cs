﻿namespace POC.WebApp.Rpc
{
    public abstract class Response
    {
        public string ErrorMessage { get; set; } = String.Empty;
        public Guid TraceId { get; set; } = Guid.Empty;
    }

    public abstract class Response<TModel> : Response
        where TModel : new()
    {
        public TModel? Payload { get; set; }
    }
}
