﻿namespace POC.WebApp.Models.Enums
{
    public enum Gender
    {
        Unset = 0,
        Male,
        Female
    }
}
