﻿using Microsoft.AspNetCore.Mvc;

using POC.People.Service.Models;
using POC.People.Service.Services;

using static System.FormattableString;

namespace POC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly ILogger<PeopleController> logger;
        private readonly IPeopleService peopleService;

        public PeopleController(
            IPeopleService peopleService,
            ILogger<PeopleController> logger)
        {
            this.logger = logger;
            this.peopleService = peopleService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPeople(CancellationToken cancellationToken)
        {
            this.logger.LogInformation(Invariant($"{nameof(GetPeople)} called"));

            ICollection<Person> people = await this.peopleService
                .GetPeopleAsync(cancellationToken)
                .ConfigureAwait(false);

            return this.Ok(people);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPerson(int id, CancellationToken cancellationToken)
        {
            this.logger.LogInformation(Invariant($"{nameof(GetPerson)} called"));

            Person? person = await this.peopleService
                .GetPersonAsync(id, cancellationToken)
                .ConfigureAwait(false);

            return person is null ? this.NotFound() : this.Ok(person);
        }

        [HttpPost]
        public async Task<IActionResult> SavePerson([FromBody] Person person, CancellationToken cancellationToken)
        {
            this.logger.LogInformation(Invariant($"{nameof(SavePerson)} called"));

            await this.peopleService
                .CreatePersonAsync(person, cancellationToken)
                .ConfigureAwait(false);

            return this.Created($"/api/people/{person.Id}", person);
        }
    }
}
