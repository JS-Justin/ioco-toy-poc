﻿using POC.People.Service.Models;

namespace POC.People.Service.Services
{
    public interface IPeopleService
    {
        Task<int> CreatePersonAsync(Person person);
        Task<int> CreatePersonAsync(Person person, CancellationToken cancellationToken);
        Task<List<Person>> GetPeopleAsync();
        Task<List<Person>> GetPeopleAsync(CancellationToken cancellationToken);
        Task<Person?> GetPersonAsync(long id);
        Task<Person?> GetPersonAsync(long id, CancellationToken cancellationToken);
    }
}