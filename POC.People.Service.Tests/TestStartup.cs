﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace POC.People.Service.Tests
{
    internal class TestStartup
    {
        public IServiceProvider ServiceProvider { get; }

        public TestStartup()
        {
            var configuration = new ConfigurationBuilder().Build();
            var startup = new Startup(configuration);

            var services = new ServiceCollection();
            startup.ConfigureServices(services);

            ServiceProvider = services.BuildServiceProvider();
        }
    }
}
