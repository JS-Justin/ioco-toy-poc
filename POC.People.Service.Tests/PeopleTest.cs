using NUnit.Framework;
using POC.People.Service.Services;
using POC.People.Service.Models;
using Microsoft.Extensions.DependencyInjection;

namespace POC.People.Service.Tests
{
    public class Tests
    {
        private IPeopleService _peopleService;
        private Person person = new Person() { Id = 1, Name = "Willie", Gender = Gender.Male, IsAlive = true };

        [SetUp]
        public void Setup()
        {
            var testStartup = new TestStartup();
            _peopleService = testStartup.ServiceProvider.GetRequiredService<IPeopleService>();
        }

        [Test]
        public async Task TestCreatePerson()
        {
            var result = await _peopleService.CreatePersonAsync(person);

            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public async Task TestGetPeople()
        {
            await _peopleService.CreatePersonAsync(person);

            var result = await _peopleService.GetPeopleAsync();

            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result[0].Id, Is.EqualTo(1));
            Assert.That(result[0].Name, Is.EqualTo("Willie"));
            Assert.That(result[0].Gender, Is.EqualTo(Gender.Male));
            Assert.That(result[0].IsAlive, Is.True);
        }

        [Test]
        public async Task TestGetPersonById_Found()
        {
            await _peopleService.CreatePersonAsync(person);

            var result = await _peopleService.GetPersonAsync(1);

            Assert.That(result?.Id, Is.EqualTo(1));
            Assert.That(result?.Name, Is.EqualTo("Willie"));
            Assert.That(result?.Gender, Is.EqualTo(Gender.Male));
            Assert.That(result?.IsAlive, Is.True);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TestGetPersonById_NotFound(bool peopleExist)
        {
            if(peopleExist)
                await _peopleService.CreatePersonAsync(person);

            var result = await _peopleService.GetPersonAsync(55);

            Assert.IsNull(result);

        }
    }
}