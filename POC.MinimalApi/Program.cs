using POC.People.Service;
using POC.People.Service.Models;
using POC.People.Service.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Register and configure People.Service class library services
var startupPeopleService = new Startup(builder.Configuration);
startupPeopleService.ConfigureServices(builder.Services);

builder.Services.AddCors(options => options
    .AddDefaultPolicy(policy => policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod())
);

var app = builder.Build();
app.UseCors();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//GET all people from the Person DB
app.MapGet("/api/people", async (IPeopleService ppSvc) =>
    await ppSvc.GetPeopleAsync() 
    );

//POST to create a new Person in Person DB
app.MapPost("/api/people", async (Person person, IPeopleService ppSvc) =>
    {
        await ppSvc.CreatePersonAsync(person);

        return Results.Created($"/api/people/{person.Id}", person);
    });

//GET a specific Person using their ID
app.MapGet("/api/people/{id}", async (long id, IPeopleService ppSvc) =>
        await ppSvc.GetPersonAsync(id)
    );

app.Run();