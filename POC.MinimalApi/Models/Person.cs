﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace POC.MinimalApi.Models
{
    public class Person
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }

        public Gender Gender { get; set; }

        public bool IsAlive { get; set; }
    }
}
