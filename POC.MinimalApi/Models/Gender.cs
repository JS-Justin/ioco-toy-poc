﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace POC.MinimalApi.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Gender
    {
        [EnumMember(Value = "Unset")]
        Unset,
        [EnumMember(Value = "Male")]
        Male,
        [EnumMember(Value = "Female")]
        Female,
    }
}
